FROM node as build

COPY . /app
WORKDIR /app

RUN npm install && \
    npm run ng build --prod

FROM nginx:alpine

COPY --from=build /app/dist/fuse /usr/share/nginx/html
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
