# CBRE

## Squeleton

* Backend: *Spring Boot*
* Frontend: *Angular*
* Database: *postgreSQL*
* Packaging: *Kubernetes*

## Project structure

```mermaid
graph TD

ingress --> frontend

ingress --> api

api --> database

api --> efk
```

## Frontend structure

* >optional: dark mode

## Backend structure

### Subdomain Customization

* page to create new subdomain

    * url
    * company information

* page to save UI prefence for subdomain

    * logo
    * background
    * header

### Authentication

* Use oAuth2 with external provider:

    * *Faceboot*
    * *Google*
    * *GitHub*
    * *Azure*
    * *Twitter*

* >optional: create internal oAuth2 provider

* >optional: use *SAML* protocol

* GDPR complient:

    * create profile page
    * mandatory: (https://bohzo.developpez.com/rgpd-guide-pratique-developpeurs/)

* Roles:

    * admin
    * user
    * domain_admin
    * domain_user


    => Attention:
    
    * 1 role allowed per user
    * 1 domain per user

* Survey

    * form to create simple survey:

        * add new question + response = 0..10

    * >optional: form to create complexe survey

        sample: https://surveyjs.io/create-survey#example

    * report JSON
    * report EXCEL

* >optional: Newsletter creator
