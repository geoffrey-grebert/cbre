import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { FuseSharedModule } from '@fuse/shared.module';

import { ForgotPassword3Component } from 'app/main/pages/authentication/forgot-password-3/forgot-password-3.component';

const routes = [
    {
        path     : 'auth/forgot-password-3',
        component: ForgotPassword3Component
    }
];

@NgModule({
    declarations: [
        ForgotPassword3Component
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,

        FuseSharedModule,
    ]
})
export class ForgotPassword3Module
{
}
